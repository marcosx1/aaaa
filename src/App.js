import React, {Component} from 'react';
import './App.css';
import Person from './Person/Person'

class App extends Component{
    state = {
        persons: [
            {name:'Marcos',age:24},
            {name:'Max',age:30},
            {name:'Hector',age:27}
        ]
    };
    switchNameHandler = () => {
        //console.log(' was clicked!');
        //Dont do this this.state.persons[0].name = 'Edgar';
        this.setState({ persons: [
                {name:'AAA',age:24},
                {name:'Max',age:30},
                {name:'Hector',age:27}
            ]})

    };
    render() {
        return (
            <div className="App">
                <h1>Hi, I'm a React App</h1>
                <button onClick={this.switchNameHandler}>Switch Name</button>
                <Person name={this.state.persons[0].name} age={this.state.persons[0].age}/>
                <Person name={this.state.persons[1].name} age={this.state.persons[1].age}/>
                <Person name={this.state.persons[2].name} age={this.state.persons[2].age}>My hobbies: Video Games</Person>
            </div>
        );
    }
}

export default App;
